import os, sys

PROJECT_DIR = '/var/www/html/maxcurvature'

activate_this = os.path.join(PROJECT_DIR, 'maxcurvature-virtualenv', 'bin', 'activate_this.py')
execfile(activate_this, dict(__file__=activate_this))
sys.path.append(PROJECT_DIR)

from maxcurvature import application
