from flask import Flask
from flask import request
from flask import render_template
import numpy as np

application = Flask(__name__)

def find_curvature(data):
  a = np.array(data)

  # First derivative
  dx_dt = np.gradient(a[:, 0])
  dy_dt = np.gradient(a[:, 1])
  velocity = np.array([ [dx_dt[i], dy_dt[i]] for i in range(dx_dt.size)])
  ds_dt = np.sqrt(dx_dt * dx_dt + dy_dt * dy_dt)

  tangent = np.array([1/ds_dt] * 2).transpose() * velocity

  # Second derivative
  # d_tangentx_dt = np.gradient(tangent[:, 0])
  # d_tangent_dy = np.gradient(tangent[:, 1])
  # dT_dt = np.array([[d_tangentx_dt[i], d_tangent_dy[i]] for i in range(d_tangentx_dt.size)])
  # length_dT_dt = np.sqrt(d_tangentx_dt * d_tangentx_dt + d_tangent_dy * d_tangent_dy)

  # normal = np.array([1/length_dT_dt] * 2).transpose() * dT_dt

  d2s_dt2 = np.gradient(ds_dt)
  d2x_dt2 = np.gradient(dx_dt)
  d2y_dt2 = np.gradient(dy_dt)

  curvature = np.abs(d2x_dt2 * dy_dt - dx_dt * d2y_dt2) / (dx_dt * dx_dt + dy_dt * dy_dt)**1.5
  return curvature

  #print curvature
  #print max(curvature)
  # t_component = np.array([d2s_dt2] * 2).transpose()
  # n_component = np.array([curvature * ds_dt * ds_dt] * 2).transpose()

  # acceleration = t_component * tangent + n_component * normal%  

  #5 12.5 25 50 100 200 400 800 1600 3200
  #0.493 0.489 0.479 0.468 0.453 0.432 0.411 0.382 0.350 0.314

@application.route('/')
def main_form():
  return render_template("input_data.html")

@application.route('/', methods=['POST'])
def my_form_post():
  x_values = [float(i) for i in request.form['x'].split(" ")]
  y_values = [float(i) for i in request.form['y'].split(" ")]
  data = [list(t) for t in zip(x_values, y_values)]
  curvature = find_curvature(data)
  max_curvature = max(curvature)
  return_value="<table border='1'>"
  return_value+="<tr><td>x</td><td>y</td><td>curvature</td></tr>"
  for i in range(len(x_values)):
    return_value+="<tr>"
    return_value+="<td>{}</td>".format(x_values[i])
    return_value+="<td>{}</td>".format(y_values[i])
    if max_curvature == curvature[i]:
      return_value+="<td bgcolor='#FF0000'>{}</td>".format(curvature[i])
    else:
      return_value+="<td>{}</td>".format(curvature[i])
    return_value+="</tr>"
  return_value+="</table>"
  return return_value

if __name__ == '__main__':
  # application.debug = True
  application.run()
